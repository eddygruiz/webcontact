// <auto-generated />
namespace WebContact.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class _true : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(_true));
        
        string IMigrationMetadata.Id
        {
            get { return "201905182218359_true"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
